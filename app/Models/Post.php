<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'content',
        'view_count',
    ];

    public function author(){
        return $this->hasMany(Author::class);
    }

    public function tag(){
        return $this->hasMany(Tag::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Categories::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
