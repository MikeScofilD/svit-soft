<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Http\Requests\ContactRequest;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return PostResource::collection(Post::all());
    }

    public function show($id)
    {
        $post = Post::find($id);
        return new PostResource(Post::findOrFail($id));
    }
}
