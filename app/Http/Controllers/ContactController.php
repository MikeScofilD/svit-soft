<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FormContact;
use App\Http\Resources\ContactResource;
use App\Http\Requests\ContactRequest;

class ContactController extends Controller
{
    public function store(ContactRequest $request)
    {
        return ContactResource::collection(FormContact::create($request->validated()));
    }
}
