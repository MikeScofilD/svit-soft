<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'author_id' => $this->faker->numberBetween(1,10),
            'tag_id' => $this->faker->numberBetween(1,10),
            'title' => $this->faker->text,
            'content' => $this->faker->sentence,
            'view_count' => $this->faker->numberBetween(0,1000),
        ];
    }
}
