<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware(['admin'])->group(function () {
    Route::apiResource('author', \App\Http\Controllers\Admin\AdminAuthorController::class);
    Route::apiResource('post', \App\Http\Controllers\Admin\AdminPostController::class);
    Route::apiResource('tag', \App\Http\Controllers\Admin\AdminTagController::class);
});
Route::apiResource('author', \App\Http\Controllers\AuthorController::class)->only('index', 'show');
Route::apiResource('post', \App\Http\Controllers\PostController::class)->only('index', 'show');
Route::apiResource('contact', \App\Http\Controllers\ContactController::class)->only('store');